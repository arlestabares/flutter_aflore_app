import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/home/home_movie_screen.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final favoriteList = Provider.of<ServicesProvider>(context).favoriteList;
    return Scaffold(
     backgroundColor: Colors.white.withOpacity(0.5),
      appBar: AppBar(
        title: const Text('Movie'),
        centerTitle: true,
        backgroundColor: Colors.black.withOpacity(0.1),
        actions: [
          IconButton(
            icon: const Icon(Icons.favorite),
            onPressed: () {
              if (favoriteList.isNotEmpty) {
                Navigator.pushNamed(context, "favoriteMovies");
            

              }
            },
          ),
        ],
      ),
      body: const HomeMoviesScreen(),
    );
  }
}
