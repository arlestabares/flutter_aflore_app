import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';

class ServicesProvider with ChangeNotifier {
  // final String _apiKey = '0d677b16a44d2b5a79edf325bc1ee9b7';
  final String _apiKey = '994983fbf90eaefe54618b962f337902';
  final String _url = 'api.themoviedb.org';
  final String _language = 'es-ES';
  final String _popularity = 'popularity.desc';

  List<Pelicula> _favoriteList = [];
  List<Pelicula> _popularesList = [];
  List<Pelicula> _movieList =[];

  List<Pelicula> get favoriteList => _favoriteList;
  List<Pelicula> get movieList => _movieList;

  bool _cargando = false;

  void addMovie(Pelicula pelicula) {
    _favoriteList.add(pelicula);
    notifyListeners();
  }

  void addMovieForId(int index, Pelicula pelicula) {
    _favoriteList.insert(index, pelicula);
    notifyListeners();
  }

  void remoteMovie(int index) {
    _favoriteList.removeAt(index);
    notifyListeners();
  }

  void remoteMovieForMovie(Pelicula pelicula) {
    _favoriteList.remove(pelicula);
    notifyListeners();
  }

  void removeMovieById(int id) {
    _favoriteList.removeWhere((element) => element.id == id);
    notifyListeners();
  }

  void remoteAllFavorites() {
    _favoriteList = [];
    notifyListeners();
  }

//Tendra multiples lugares escuchando la misma vision o flujo
  final _popularesStreamController =
      StreamController<List<Pelicula>>.broadcast();

//Introducimos la lista de peliculas al controller
  Function(List<Pelicula>) get getPopularesSink =>
      _popularesStreamController.sink.add;

//Introducimos la lista de peliculas al Stream  para que las escuche.
  Stream<List<Pelicula>> get getPopularesStream =>
      _popularesStreamController.stream;

//Cerramos el flujo cuando no esta activo, optimizando memoria
  void disposeStreams() {
    _popularesStreamController.close();
  }


  Future<List<Pelicula>> getPopulares() async {
    if (_cargando) return [];
    _cargando = true;
    final url = Uri.https(_url, '3/discover/movie/', {
      'api_key': _apiKey,
      'language': _language,
      'short_by': _popularity,
    });
    final response = await _processResponse(url);
    _cargando = false;
    _movieList.addAll(response);
    _popularesList.addAll(response);
     getPopularesSink(_popularesList);
    notifyListeners();
    return response;
  }

  ///*
  ///* Future que procesa la solicitud
  ///* url, parametro
  Future<List<Pelicula>> _processResponse(Uri url) async {
    try {
      final response = await http.get(url);
      final decodedData = json.decode(response.body);
      final peliculas = Peliculas.fromJsonList(decodedData['results']);
      return peliculas.items!;
    } catch (e) {
      return throw Exception("Algo salio mal");
    }
  }
}
