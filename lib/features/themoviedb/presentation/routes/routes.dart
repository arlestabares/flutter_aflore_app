import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/pages/home_page.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/details/movie_detail.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/favorites/details/favorites_details.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/favorites/favorites_screen.dart';

final Map<String, Widget Function(BuildContext)> appRoutes = {
  'homePage': (_) => const HomePage(),
  'detalle': (_) => const MovieDetails(),
  'detallesFavoritos': (_) => const FavoritesDetails(),
  'favoriteMovies':(_)=>const FavoriteScreen()
};
