import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';
import 'package:provider/provider.dart';

class BuildAppBarDetail extends StatelessWidget {
  final Pelicula? pelicula;

  const BuildAppBarDetail({Key? key, required this.pelicula}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final servicesProvider = Provider.of<ServicesProvider>(context);
    final _sizeScreen = MediaQuery.of(context).size;
    return SliverAppBar(
      elevation: 2.0,
      backgroundColor: Colors.indigoAccent,
      expandedHeight: _sizeScreen.height * 0.35,
      floating: false,
      pinned: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text(
          pelicula!.title!,
          style: const TextStyle(color: Colors.white, fontSize: 18.0),
        ),
        background: FadeInImage(
          image: NetworkImage(pelicula!.getBackgroundImg()),
          placeholder: const AssetImage('assets/image/loading.gif'),
          fadeOutDuration: const Duration(milliseconds: 200),
          fit: BoxFit.cover,
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: IconButton(
            icon: const Icon(Icons.favorite),
            onPressed: () {
              
              if (servicesProvider.favoriteList.contains(pelicula)) {
                servicesProvider.favoriteList.remove(pelicula);
              }else{
                  servicesProvider.addMovie(pelicula!);

              }

              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text("Guardado en Favoritos"),
                  duration: const Duration(seconds: 2),
                  action: SnackBarAction(
                    label: pelicula!.title!,
                    onPressed: () {},
                  ),
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
