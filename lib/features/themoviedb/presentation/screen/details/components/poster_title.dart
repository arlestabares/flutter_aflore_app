import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';

class PosterTitle extends StatelessWidget {
  final Pelicula pelicula;

  const PosterTitle({Key? key, required this.pelicula}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    
    return Container(
      margin: const EdgeInsets.only(left: 5.0),
      child: Row(
        children: [
          Hero(
            tag: pelicula.id!,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image(
                image: NetworkImage(
                  pelicula.getPosterImg(),
                ),
                height: _screenSize.height * 0.15,
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  pelicula.title!,
                  style: Theme.of(context).textTheme.headline6,
                  overflow: TextOverflow.ellipsis,
                ),
                Text(
                  pelicula.originalTitle!,
                  style: Theme.of(context).textTheme.subtitle1,
                ),
                Row(
                  children: <Widget>[
                   const Icon(Icons.star_border),
                    Text(
                      pelicula.voteAverage.toString(),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
