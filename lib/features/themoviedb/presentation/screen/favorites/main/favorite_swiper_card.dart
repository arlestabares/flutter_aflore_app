import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';
import 'package:provider/provider.dart';

class FavoriteSwiperCard extends StatelessWidget {
  final _pageController = PageController(
    initialPage: 1,
    // viewportFraction: 0.8,
  );

  FavoriteSwiperCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 300) {
        // siguientePagina();
      }
    });
    return Consumer<ServicesProvider>(
      builder: (context, provider, child) {
        return Container(
          padding: const EdgeInsets.only(top: 2.0),
          child: provider.favoriteList.isNotEmpty
              ? Swiper(
                  layout: SwiperLayout.STACK,
                  itemWidth: _screenSize.width * 0.75,
                  itemCount: provider.favoriteList.length,
                  itemBuilder: (BuildContext context, index) {
                    return ClipRRect(
                      borderRadius: BorderRadius.circular(20.0),
                      child: GestureDetector(
                        onTap: () => Navigator.pushNamed(
                          context,
                          'detallesFavoritos',
                          arguments: provider.favoriteList[index],
                        ),
                        child: FadeInImage(
                          image: NetworkImage(
                              provider.favoriteList[index].getPosterImg()),
                          placeholder:
                              const AssetImage('assets/image/no-image.jpg'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    );
                  },
                )
              : const SizedBox(),
        );
      },
    );
  }
}
