import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';

import 'components/build_appbar_detail.dart';

class FavoritesDetails extends StatelessWidget {
  const FavoritesDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Pelicula pelicula =
        ModalRoute.of(context)!.settings.arguments as Pelicula;
    return SafeArea(
      child: Scaffold(
        body: CustomScrollView(
          slivers: [
            FavoritesBuildAppBarDetail(pelicula: pelicula),
            SliverList(
              delegate: SliverChildListDelegate([
                const SizedBox(height: 10.0),
                // PosterTitle(pelicula: pelicula),
                DescriptionDetails(pelicula: pelicula),
                const SizedBox(height: 20.0),
                DescriptionDetails(pelicula: pelicula),
                const SizedBox(height: 20.0),
                DescriptionDetails(pelicula: pelicula),
                const SizedBox(height: 20.0),
                DescriptionDetails(pelicula: pelicula),
                DescriptionDetails(pelicula: pelicula),
              ]),
            )
          ],
        ),
      ),
    );
  }
}

class DescriptionDetails extends StatelessWidget {
  final Pelicula pelicula;

  const DescriptionDetails({Key? key, required this.pelicula})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Text(
        pelicula.overview!,
        textAlign: TextAlign.justify,
        style: const TextStyle(color: Colors.black, fontSize: 18.0),
      ),
    );
  }
}
