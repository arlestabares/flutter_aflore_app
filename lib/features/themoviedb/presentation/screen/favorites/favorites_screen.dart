import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';
import 'package:provider/provider.dart';

import 'main/favorite_swiper_card.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final favoriteList = Provider.of<ServicesProvider>(context);
    return Scaffold(
      backgroundColor: Colors.white.withOpacity(0.5),
        appBar: AppBar(
          backgroundColor: Colors.black.withOpacity(0.1),
          title: const Text("Favorites"),
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: const Icon(Icons.arrow_back),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.delete_forever),
              onPressed: () {
                favoriteList.remoteAllFavorites();
              },
            ),
          ],
        ),
        body: _favoriteCardList());
  }

  Widget _favoriteCardList() {
    return FavoriteSwiperCard();
  }
}
