import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';

class MainCardHeroSwiper extends StatelessWidget {
  final List<Pelicula>? peliculas;
  final _pageController = PageController(
    initialPage: 1,
    // viewportFraction: 0.8,
  );

  MainCardHeroSwiper({Key? key, this.peliculas}) : super(key: key);
  // HeroSwiperCard({Key? key, required this.peliculas}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 300) {
        // siguientePagina();
      }
    });
    return Container(
      // height: _screenSize.height * 0.8,
      padding: const EdgeInsets.only(top: 2.0),
      child: peliculas!.isNotEmpty
          ? Swiper(
              layout: SwiperLayout.STACK,
              itemWidth: _screenSize.width * 0.8,
              itemHeight: _screenSize.height * 0.48,
              itemCount: peliculas!.length,
              itemBuilder: (BuildContext context, index) {
                return Hero(
                  tag: peliculas![index].id!,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20.0),
                    child: GestureDetector(
                      onTap: () => Navigator.pushNamed(
                        context,
                        'detalle',
                        arguments: peliculas![index],
                      ),
                      child: FadeInImage(
                        image: NetworkImage(peliculas![index].getPosterImg()),
                        placeholder:
                            const AssetImage('assets/image/no-image.jpg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                );
              },
            )
          : const Center(
              child: SizedBox(
                child: CircularProgressIndicator(),
              ),
            ),
    );
  }
}
