import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';

import 'main_card_hero_swiper.dart';

class MainCardSwiperList extends StatelessWidget {
  const MainCardSwiperList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final movieProvider = Provider.of<ServicesProvider>(context);

    return FutureBuilder(
      future: movieProvider.getPopulares(),
      builder: (context, AsyncSnapshot<List<Pelicula>> snapshot) {
        if (snapshot.hasData) {
          return MainCardHeroSwiper(peliculas: snapshot.data);
        } else {
          return const SizedBox(
            height: 100,
            child: Center(child: CircularProgressIndicator()),
          );
        }
      },
      
    );
  }
}
