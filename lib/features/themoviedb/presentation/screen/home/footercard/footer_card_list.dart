import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';
import 'package:provider/provider.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';

import 'components/page_view_horizontal_movie.dart';

class FooterCardList extends StatelessWidget {
  const FooterCardList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final movieProvider = Provider.of<ServicesProvider>(context);
    return Container(
      padding:const EdgeInsets.symmetric(horizontal:10.0),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 20.0),
            // margin: const EdgeInsets.symmetric(horizontal: 10.0),
            child: Text(
              'Populares',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          const SizedBox(height: 20.0),
          StreamBuilder(
            stream: movieProvider.getPopularesStream,
            builder: (context, AsyncSnapshot<List<Pelicula>> snapshot) {
              if (snapshot.hasData) {
                return PageViewHorizontalMovie(
                  peliculas: snapshot.data,
                  siguientePagina: movieProvider.getPopulares,
                );
              } else {
                const Center(child: CircularProgressIndicator());
              }
              return const SizedBox();
            },
          )
        ],
      ),
    );
  }
}
