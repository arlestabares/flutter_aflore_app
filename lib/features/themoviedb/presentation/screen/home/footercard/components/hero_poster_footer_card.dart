import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';

class HeroPosterFooterCard extends StatelessWidget {
  final Pelicula pelicula;
  const HeroPosterFooterCard({
    Key? key,
    required this.pelicula,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;
    pelicula.uniquedId = '${pelicula.id}-poster';
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, 'detalle', arguments: pelicula);
      },
      child: Container(
        margin: const EdgeInsets.only(right: 15.0),
        child: Column(
          children: <Widget> [
            Hero(
              tag: pelicula.uniquedId!,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  image: NetworkImage(pelicula.getPosterImg()),
                  placeholder: const AssetImage('assets/image/no-image.jpg'),
                  fit: BoxFit.cover,
                  height: _screenSize.height * 0.15,
                ),
              ),
            ),
            const SizedBox(),
            Text(
              pelicula.title!,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.caption,
            )
          ],
        ),
      ),
    );
  }
}
