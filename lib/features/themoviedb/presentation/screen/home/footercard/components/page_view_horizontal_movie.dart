import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/data/models/pelicula_model.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/home/footercard/components/hero_poster_footer_card.dart';

class PageViewHorizontalMovie extends StatelessWidget {
  final List<Pelicula>? peliculas;
  final Function siguientePagina;
  final _pageController = PageController(
    initialPage: 1,
    viewportFraction: 0.3,
  );

  PageViewHorizontalMovie({
    Key? key,
    required this.peliculas,
    required this.siguientePagina,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 200) {
        siguientePagina;
      }
    });
    return SizedBox(
      height: _screenSize.height * 0.27,
      child: PageView.builder(
        pageSnapping: false,
        controller: _pageController,
        itemCount: peliculas!.length,
        itemBuilder: (context, index) {
          return HeroPosterFooterCard(pelicula: peliculas![index]);
        },
      ),
    );
  }
}
