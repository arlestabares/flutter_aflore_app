import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/screen/home/footercard/footer_card_list.dart';

import 'maincard/main_card_swiper_list.dart';

class HomeMoviesScreen extends StatelessWidget {
  const HomeMoviesScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      height: _screenSize.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const <Widget>[
          // SizedBox(height: 80.0),
          MainCardSwiperList(),
          FooterCardList()
        ],
      ),
    );
  }
}
