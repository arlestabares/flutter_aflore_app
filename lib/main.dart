import 'package:flutter/material.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/provider/services_provider.dart';
import 'package:flutter_aflore_app/features/themoviedb/presentation/routes/routes.dart';
import 'package:provider/provider.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => ServicesProvider(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Material App',
        initialRoute: 'homePage',
        routes: appRoutes,
      ),
    );
  }
}
